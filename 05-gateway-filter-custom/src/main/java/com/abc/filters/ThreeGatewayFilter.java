package com.abc.filters;

/*
 * @Author yaxiongliu
 **/

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 目标：查看过滤器执行顺序，观察pre过滤和post过滤
 */
//过滤器3
public class ThreeGatewayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取系统当前时间
        System.out.println("pre-filter-【333】 " );
        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            System.out.println("post-filter-【333】 ");
        }));
    }
}
