package com.abc.filters;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/*
 * @Author yaxiongliu
 * 目标：实现需求，在自定义的 Filter 中为请求添加指定的请求头。
 **/
public class AddHeaderGatewayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取请求对象，改造请求对象的请求头
        ServerHttpRequest request = exchange
                .getRequest()
                .mutate()
                .header("x-header","blue").build();
        //2.将请求头设置会当前exchange
        ServerWebExchange exchange1 = exchange.mutate().request(request).build();
        //3.请求放行
        return chain.filter(exchange1);
    }

}
