package com.abc.filters;

/*
 * @Author yaxiongliu
 **/

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 目标：查看过滤器执行顺序，观察pre过滤和post过滤
 */
//过滤器1
public class OneGatewayFilter implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //-------------前置过滤器开始
        //获取系统当前时间
        long startTime = System.currentTimeMillis();
        System.out.println("pre-filter-【111】 " );
        System.out.println("入参 token = " + exchange.getRequest().getQueryParams().get("token"));
        //设置filter过滤器时间
        exchange.getAttributes().put("startTime", startTime);
        //--------------前置过滤器结束


        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            //-------------后置过滤器开始
            System.out.println("post-filter-【111】 ");
            //获取过滤器执行开始时间
            Long startTimeAttr = (Long) exchange.getAttributes().get("startTime");
            //获取过滤器执行结束时间
            System.out.println("返参 = " + exchange.getResponse());
            long endTime = System.currentTimeMillis();
            //计算开始到结束时间差值
            System.out.println("OneGatewayFilter过滤器执行用时：" + (endTime - startTime));
            //-------------后置过滤器结束
        }));
    }
}
