package com.abc;

import com.abc.filters.AddHeaderGatewayFilter;
import com.abc.filters.OneGatewayFilter;
import com.abc.filters.ThreeGatewayFilter;
import com.abc.filters.TwoGatewayFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class GatewayFilterCustomApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayFilterCustomApplication.class, args);
    }
    //配置令牌桶算法的key:将主机名称作为限流key
    @Bean
    public KeyResolver keyResolver(){
        return exchange -> Mono.just(exchange
                .getRequest()
                .getRemoteAddress()
                .getHostName());
    }
    //配置路由：自定义过滤器
//    @Bean
//    public RouteLocator someRouteLocator(RouteLocatorBuilder builder) {
//        //路由构建器对象，构建一个路由规则
//        return builder.routes().route(predicateSpec -> predicateSpec
//                .path("/**")
//                .filters(gfs -> gfs.filter(new AddHeaderGatewayFilter()))
//                .uri("http://localhost:8080")
//                .id("AddHeader_route")).build();
//    }

//    @Bean
//    public RouteLocator someRouteLocator(RouteLocatorBuilder builder) {
//        //路由构建器对象，构建一个路由规则
//        return builder.routes().route(predicateSpec -> predicateSpec
//                .path("/**")
//                .filters(gfs -> gfs
//                        .filter(new OneGatewayFilter())
//                        .filter(new TwoGatewayFilter())
//                        .filter(new ThreeGatewayFilter()))
//                .uri("http://localhost:8080")
//                .id("three_route")).build();
//    }

}
