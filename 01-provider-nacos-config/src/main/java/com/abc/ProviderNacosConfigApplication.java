package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderNacosConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderNacosConfigApplication.class, args);
    }

}
