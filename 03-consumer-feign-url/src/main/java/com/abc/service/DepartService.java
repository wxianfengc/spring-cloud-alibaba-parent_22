package com.abc.service;

import com.abc.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 访问百度
 */
@FeignClient(name = "go-baidu"
        , url = "www.baidu.com"
        , configuration = FeignConfiguration.class)
public interface DepartService {
    //html代码字符串：一切数据皆可字符串【001010】
    @GetMapping("/")
    String goBaidu();
}
