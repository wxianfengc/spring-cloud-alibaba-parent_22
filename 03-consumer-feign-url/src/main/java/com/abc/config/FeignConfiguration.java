package com.abc.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


/*
 * @Author yaxiongliu
 **/
@Configuration
public class FeignConfiguration {

    //注入配置的属性值
    @Bean
    public Logger.Level feignLoggerConfig(){
        return Logger.Level.FULL;//打印所有信息
    }

}
