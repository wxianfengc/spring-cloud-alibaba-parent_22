package com.abc.controller;

import com.abc.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DepartFeignController {

    @Autowired
    private DepartService departService;
    //访问百度服务
    @GetMapping("/goBaidu")
    public String getHandle() {
        return departService.goBaidu();
    }
}
