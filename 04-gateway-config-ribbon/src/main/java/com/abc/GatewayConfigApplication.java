package com.abc;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class GatewayConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayConfigApplication.class, args);
    }
    //配置负载均衡策略
    @Bean
    public IRule loadBalanceRule(){
        return new RandomRule();
    }
}
