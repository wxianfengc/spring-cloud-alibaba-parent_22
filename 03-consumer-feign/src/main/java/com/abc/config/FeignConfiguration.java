package com.abc.config;

import com.abc.custom.CustomRule;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


/*
 * @Author yaxiongliu
 **/
@Configuration
public class FeignConfiguration {

    //注入配置的属性值
    @Bean
    public Logger.Level feignLoggerConfig(){
        return Logger.Level.FULL;//打印所有信息
    }

    //配置负载均衡策略
//    @Bean
//    public IRule loadBalanceRule(){
//        return new RandomRule();
//    }
    //配置自定义负载均衡器
    @Bean
    public IRule loadBalanceRule(){
        List<Integer> ports = new ArrayList<>();
        ports.add(8081);
        return new CustomRule(ports);
    }
}
