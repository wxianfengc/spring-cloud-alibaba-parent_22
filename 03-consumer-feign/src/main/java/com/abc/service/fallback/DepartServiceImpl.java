package com.abc.service.fallback;

import com.abc.bean.Depart;
import com.abc.service.DepartService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/*
 * @Author yaxiongliu
 **/
@Component//注意，服务降级处理类，需要注入Spring的容器
@RequestMapping("/fallback/provider/depart")
public class DepartServiceImpl implements DepartService {
    @Override
    public boolean saveDepart(Depart depart) {
        return false;
    }

    @Override
    public boolean removeDepartById(int id) {
        return false;
    }

    @Override
    public boolean modifyDepart(Depart depart) {
        return false;
    }

    @Override
    public Depart getDepartById(int id) {
        Depart depart = new Depart();
        depart.setName("null");
        return depart;
    }

    @Override
    public List<Depart> listAllDeparts() {
        return null;
    }
}
