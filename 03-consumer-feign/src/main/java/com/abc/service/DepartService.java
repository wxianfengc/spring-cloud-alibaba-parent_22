package com.abc.service;

import com.abc.bean.Depart;
import com.abc.config.FeignConfiguration;
import com.abc.service.fallback.DepartServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 负载均衡器的代码实现，是基于Ribbon内负载代码逻辑实现。
 * 负载均衡器需要依赖于注册中心Nacos的注册列表，但并不是Nacos注册中心做的！
 */

//如果ip写死了就没有意义了，
//Feign客户端可以像RestTemplate一样发送http代码
//一般情况下会通过服务名称，DNS去访问
//当前接口称之为：声明式REST客户端
//fallback属性：设置服务降级处理类
//value属性：设置被方位服务名称
//configuration属性：设置feign客户端配置类
@FeignClient(value = "msc-provider-service"
        ,fallback = DepartServiceImpl.class
        ,configuration = FeignConfiguration.class)
@RequestMapping("/provider/depart")
public interface DepartService {

    //URL 1  http://msc-provider-service/provider/depart?id=111&name=11
    @PostMapping("/save")
    boolean saveDepart(@RequestBody Depart depart);
    //URL 2  http://msc-provider-service/del/{id}
    @DeleteMapping("/del/{id}")
    boolean removeDepartById(@PathVariable("id") int id);
    @PutMapping("/update")
    boolean modifyDepart(@RequestBody Depart depart);
    @GetMapping("/get/{id}")
    Depart getDepartById(@PathVariable("id") int id);
    @GetMapping("/list")
    List<Depart> listAllDeparts();
}
