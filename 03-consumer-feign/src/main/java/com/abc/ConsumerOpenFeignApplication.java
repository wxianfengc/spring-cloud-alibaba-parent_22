package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.abc.service")
public class ConsumerOpenFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerOpenFeignApplication.class, args);
    }

    /**
     * 注入RestTemplate模板对象，用来发送请求
     */
//    @Bean
//    @LoadBalanced//注解作用：配置开启负载均衡支持
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
}
