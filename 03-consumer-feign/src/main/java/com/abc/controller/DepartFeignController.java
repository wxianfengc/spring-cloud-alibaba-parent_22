package com.abc.controller;

import com.abc.bean.Depart;
import com.abc.service.DepartService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * 消费者Controller
 * 目标：使用Feign替代RestTemplate发送http请求。
 */
@RestController
@RequestMapping("/feign/consumer/depart")
public class DepartFeignController {

    @Autowired//TODO 1.没有接口实现类，只有接口，为什么对象会出现在Spring的容器呢？
    private DepartService departService;

    /**
     * 注意：会遇到两个小问题
     * 1.问题，注入对象的时候，会识别不出到底是东动态代理生成的对象，还是自己降级类对象
     * 2.问题，Ambiguous mapping. 在降级处理类中，编写@RequestMapping注解，即可解决问题
     */

    //跨服务新增
    @PostMapping("/save")
    public boolean saveHandle(@RequestBody Depart depart) {
        return departService.saveDepart(depart);
    }
    //跨服务根据id删除
    @DeleteMapping("/del/{id}")
    public void deleteHandle(@PathVariable("id") int id) {
        departService.removeDepartById(id);
    }
    //跨服务修改
    @PutMapping("/update")
    public void updateHandle(@RequestBody Depart depart) {
        departService.modifyDepart(depart);
    }
    //跨服务根据id查询
    @GetMapping("/get/{id}")
    public Depart getHandle(@PathVariable("id") int id) {
        return departService.getDepartById(id);
    }
    //跨服务根据列表查询
    @GetMapping("/list")
    public List<Depart> listHandle() {
        return departService.listAllDeparts();
    }
}
