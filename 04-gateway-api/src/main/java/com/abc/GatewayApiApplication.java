package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApiApplication.class, args);
    }

    //配置类的方式设置路由规则

    //配置路由规则
    @Bean
    public RouteLocator someRouteLocator(RouteLocatorBuilder builder){
        return builder.routes().route(predicateSpec -> predicateSpec
                .path("/**")
                .uri("https://www.baidu.com")
                .id("baidu_route")).build();//id是当前路由的唯一标识符
    }
}
