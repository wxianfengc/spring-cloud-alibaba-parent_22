package com.abc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 消费者Controller
 */
@RestController
@RequestMapping("/info")
public class ShowinfoController {
    //获取请求头的基本信息
    @RequestMapping("header")
    public String getHeader(HttpServletRequest request) {
        return "x-header = " + request.getHeader("x-header");//x-header = blue
    }

    @RequestMapping("time")
    public String getTime(HttpServletRequest request) {
        return "time " + System.currentTimeMillis();//x-header = blue
    }

}
