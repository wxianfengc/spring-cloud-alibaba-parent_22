package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ConsumerNacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerNacosApplication.class, args);
    }

    /**
     * 注入RestTemplate模板对象，用来发送请求
     */
    @Bean
    @LoadBalanced//注解作用：配置开启负载均衡支持
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
